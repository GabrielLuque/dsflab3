package sortersTesterClasses;

import java.util.Comparator;

import sorterClasses.BubbleSortSorter;

public class IntTest {
	public static <E> void show(E[] e) {
		System.out.println("The array has the following elements:");
		for(int i=0;i<e.length;i++) {	
			System.out.print(e[i]);	
			
			if(i<e.length-1) {
				System.out.print(",");
			}
			else {
				System.out.print(".\n");
			}
		}		
		
	}
	
	
	public static void main(String[] args) {
		Integer[] testArr      = {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};
		BubbleSortSorter bSort = new BubbleSortSorter<Integer>();
	
		
		
		show(testArr);
		
		bSort.sort(testArr,null);
		
		show(testArr);
		
		
	}

}
