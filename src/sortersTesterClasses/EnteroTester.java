package sortersTesterClasses;

import java.util.Random;

import sorterClasses.BubbleSortSorter;
import sorterClasses.InsertionSortSorter;
import sorterClasses.SelectionSortSorter;

public class EnteroTester {
	public static <E> void show(E[] e) {
		System.out.println("The array has the following elements:");
		for(int i=0;i<e.length;i++) {	
			System.out.print(e[i]);	
			
			if(i<e.length-1) {
				System.out.print(",");
			}
			else {
				System.out.print(".\n");
			}
		}		
		
	}
	
	public static void test(int n) {
		Entero[] testArr = new Entero[n];
		Random rnd = new Random(n); 
		BubbleSortSorter bSort = new BubbleSortSorter<Entero>();
		SelectionSortSorter sSort = new SelectionSortSorter<Entero>();
		InsertionSortSorter iSort = new InsertionSortSorter<Entero>();
		
		
		
		
		for(int i=0; i<testArr.length;i++) {
			testArr[i] = new Entero(rnd.nextInt(n*2));
		}
		show(testArr);
		
		for(int i=0;i<3;i++) {
			Entero[] sort = testArr.clone();
			switch(i) {
			case 0:
				System.out.println("Bubble Sort:");
				bSort.sort(sort, null);
				show(sort);
				break;
			case 1:	
				System.out.println("Selection Sort:");
				sSort.sort(sort, null);
				show(sort);
				break;
			case 2:	
				System.out.println("Insertion Sort:");
				iSort.sort(sort, null);
				show(sort);
				break;
			}
			
			
		}
	
	
	
	
	
	
	}
	
	
	public static void main(String[] args) {
		
	test(10);
	}

	
	
}
